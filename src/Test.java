import com.healthinsurance.customerdetails.CurrentHealth;
import com.healthinsurance.customerdetails.Customer;
import com.healthinsurance.customerdetails.Habits;
import com.healthinsurance.utils.PremiumPercentage;
import com.healthinsurance.utils.PremiumUtils;

public class Test {

	public static void main(String[] args) {
		Customer customer = new Customer("Norman Gomes",PremiumPercentage.MALE,34);
		CurrentHealth current = new CurrentHealth(false, false, false, true);
		customer.setCurrentHealth(current);
		Habits habits = new Habits(false, true, true, false);
		customer.setHabits(habits);
		String name = customer.getName();
		String[] names = name.split(" ");
		System.out.println("Health Insurance Premium for Mr. "+names[1]+":"+PremiumUtils.getPremium(customer));
	}
}
