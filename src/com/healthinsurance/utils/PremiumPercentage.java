package com.healthinsurance.utils;

public class PremiumPercentage {

	private PremiumPercentage() {

	}

	public static final float AGE_BT_18_25 = 10;
	public static final float AGE_BT_25_30 = 20;
	public static final float AGE_BT_31_35 = 30;
	public static final float AGE_BT_36_40 = 40;
	public static final float AGE_BT_41 = 60;

	public static final float MALE_SLAB = 2;
	public static final float HEALTH_ISSUE = 1;
	public static final float GOOD_HABITS = 3;
	public static final float BAD_HABITS = 3;
	
	public static final String MALE= "MALE";
	

}
