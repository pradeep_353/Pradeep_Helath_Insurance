package com.healthinsurance.utils;

import com.healthinsurance.customerdetails.CurrentHealth;
import com.healthinsurance.customerdetails.Customer;
import com.healthinsurance.customerdetails.Habits;

public class PremiumUtils {

	private static final double BASIC_PREMIUM = 5000;

	public static final double getPremium(Customer customer) {
		
		return cacluatePremium(customer);

	}

	private static final double cacluatePremium(Customer customer) {

		double totalPremium = BASIC_PREMIUM;
		if (customer != null) {
			totalPremium += (totalPremium * agePremium(customer.getAge())) / 100;
			totalPremium += (totalPremium * currentHealth(customer.getCurrentHealth())) / 100;
			totalPremium += (totalPremium * genderSlabPremium(customer.getGender())) / 100;
			Habits habits = customer.getHabits();
			if (habits.isDailyExercise()) {
				totalPremium -= (totalPremium * PremiumPercentage.GOOD_HABITS) / 100;
			}

			if (habits.isAlcohol()) {
				totalPremium += (totalPremium * PremiumPercentage.BAD_HABITS) / 100;
			}

			if (habits.isDrugs()) {
				totalPremium += (totalPremium * PremiumPercentage.BAD_HABITS) / 100;
			}
			if (habits.isSmoking()) {
				totalPremium += (totalPremium * PremiumPercentage.BAD_HABITS) / 100;
			}
		}
		return totalPremium;
	}

	private static double genderSlabPremium(String gender) {
		float genderPremium = 0;
		if (gender.equalsIgnoreCase(PremiumPercentage.MALE)) {
			genderPremium = PremiumPercentage.MALE_SLAB;
		}
		return genderPremium;
	}

	private static final double agePremium(int age) {
		double agePremium = 0;
		if (age > 18 && age <= 25) {
			agePremium = PremiumPercentage.AGE_BT_18_25;
		} else if (age > 25 && age <= 30) {
			agePremium = PremiumPercentage.AGE_BT_25_30;
		} else if (age > 30 && age <= 35) {
			agePremium = PremiumPercentage.AGE_BT_31_35;
		} else if (age > 35 && age <= 40) {
			agePremium = PremiumPercentage.AGE_BT_36_40;
		} else {
			agePremium = PremiumPercentage.AGE_BT_41;
		}
		return agePremium;

	}

	private static final double currentHealth(CurrentHealth currentHealth) {
		double healthPremium = 0;
		if (currentHealth != null) {
			if (currentHealth.isHypertension()) {
				healthPremium += PremiumPercentage.HEALTH_ISSUE;
			}
			if (currentHealth.isBloodPressure()) {
				healthPremium += PremiumPercentage.HEALTH_ISSUE;
			}
			if (currentHealth.isBloodSugar()) {
				healthPremium += PremiumPercentage.HEALTH_ISSUE;
			}
			if (currentHealth.isOverWeight()) {
				healthPremium += PremiumPercentage.HEALTH_ISSUE;
			}
		}
		return healthPremium;
	}

}
